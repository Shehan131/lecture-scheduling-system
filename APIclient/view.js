$(document).ready(function () {

    var showData = $('#show-data');

    $.getJSON('http://localhost:3000/api/lecture', function (data) {
      console.log(data);

      var items = data.Users.map(function (item) {
        var content = '<tr><td>' + item.Lecture_id + '</td><td>'+item.Module+'</td><td>'+item.Batch+'</td><td>'+item.Course+'</td><td>'+item.Date+'</td><td>'+item.Time+'</td></tr>';
        var list = $('<tbody/>').html(content);
        showData.append(list);
      });

   
    });

  $("#Module").on("change",function(){

      var selected = this.value;

       $.getJSON('http://localhost:3000/api/lecture/module/'+selected+'', function (data) {
      console.log(data);

      $("#show-data").empty();
      var items = data.Users.map(function (item) {
        var content = '<tr><td>' + item.Lecture_id + '</td><td>'+item.Module+'</td><td>'+item.Batch+'</td><td>'+item.Course+'</td><td>'+item.Date+'</td><td>'+item.Time+'</td></tr>';
        var list = $('<tbody/>').html(content);
        showData.append(list);
    });

       });
      
      
    });

  $("#Batch").on("change",function(){

      var selected = this.value;

       $.getJSON('http://localhost:3000/api/lecture/batch/'+selected+'', function (data) {
      console.log(data);

      $("#show-data").empty();
      var items = data.Users.map(function (item) {
        var content = '<tr><td>' + item.Lecture_id + '</td><td>'+item.Module+'</td><td>'+item.Batch+'</td><td>'+item.Course+'</td><td>'+item.Date+'</td><td>'+item.Time+'</td></tr>';
        var list = $('<tbody/>').html(content);
        showData.append(list);
    });

       });
      
      
    });

  $("#Course").on("change",function(){

      var selected = this.value;

       $.getJSON('http://localhost:3000/api/lecture/course/'+selected+'', function (data) {
      console.log(data);

      $("#show-data").empty();
      var items = data.Users.map(function (item) {
        var content = '<tr><td>' + item.Lecture_id + '</td><td>'+item.Module+'</td><td>'+item.Batch+'</td><td>'+item.Course+'</td><td>'+item.Date+'</td><td>'+item.Time+'</td></tr>';
        var list = $('<tbody/>').html(content);
        showData.append(list);
    });

       });
      
      
    });

  $("#datefrom").on("change",function(){

      var selected = this.value;
     

      $.getJSON('http://localhost:3000/api/lecture/datefrom/'+selected+'', function (data) {
      console.log(data);

      $("#show-data").empty();
      var items = data.Users.map(function (item) {
        var content = '<tr><td>' + item.Lecture_id + '</td><td>'+item.Module+'</td><td>'+item.Batch+'</td><td>'+item.Course+'</td><td>'+item.Date+'</td><td>'+item.Time+'</td></tr>';
        var list = $('<tbody/>').html(content);
        showData.append(list);
    });

       });
      
      
    });

   $("#dateto").on("change",function(){

      var selected = this.value;
     

      $.getJSON('http://localhost:3000/api/lecture/dateto/'+selected+'', function (data) {
      console.log(data);

      $("#show-data").empty();
      var items = data.Users.map(function (item) {
        var content = '<tr><td>' + item.Lecture_id + '</td><td>'+item.Module+'</td><td>'+item.Batch+'</td><td>'+item.Course+'</td><td>'+item.Date+'</td><td>'+item.Time+'</td></tr>';
        var list = $('<tbody/>').html(content);
        showData.append(list); 
    });

       });
      
      
    });

    

});


var mysql = require("mysql");
function REST_ROUTER(router,connection) {
    var self = this;
    self.handleRoutes(router,connection);
}

REST_ROUTER.prototype.handleRoutes= function(router,connection) {
   

   // This insert function ends up with a mysql error

    //  router.post("/lecture/insert",function(req,res){
       
    //     var query = "INSERT INTO ??(??,??,??) VALUES (?,?,?)";
    //     var table = ["lecture","Module","Batch","Course",req.module,req.batch,req.course];
    //     console.log(req.body.module);
    //     query = mysql.format(query,table);
    //     connection.query(query,function(err,rows){
    //         if(err) {
    //             res.json({"Error" : true, "Message" : "Error executing MySQL query"});
    //         } else {
    //             res.json({"Error" : false, "Message" : "User Added !"});
    //         }
    //     });
    // });

  
   router.get("/lecture",function(req,res){
        var query = "SELECT * FROM ??";
        var table = ["lecture"];
        query = mysql.format(query,table);
        connection.query(query,function(err,rows){
            if(err) {
                res.json({"Error" : true, "Message" : "Error executing MySQL query"});
            } else {
                res.json({"Error" : false, "Message" : "Success", "Users" : rows});
            }
        });
    });

    router.get("/lecture/module/:Module_id",function(req,res){
        var query = "SELECT * FROM ?? WHERE ??=?";
        var table = ["lecture","Module",req.params.Module_id];
        query = mysql.format(query,table);
        connection.query(query,function(err,rows){
            if(err) {
                res.json({"Error" : true, "Message" : "Error executing MySQL query"});
            } else {
                res.json({"Error" : false, "Message" : "Success", "Users" : rows});
            }
        });
    });

    router.get("/lecture/batch/:Batch_id",function(req,res){
        var query = "SELECT * FROM ?? WHERE ??=?";
        var table = ["lecture","Batch",req.params.Batch_id];
        query = mysql.format(query,table);
        connection.query(query,function(err,rows){
            if(err) {
                res.json({"Error" : true, "Message" : "Error executing MySQL query"});
            } else {
                res.json({"Error" : false, "Message" : "Success", "Users" : rows});
            }
        });
    });

    router.get("/lecture/course/:Course_id",function(req,res){
        var query = "SELECT * FROM ?? WHERE ??=?";
        var table = ["lecture","Course",req.params.Course_id];
        query = mysql.format(query,table);
        connection.query(query,function(err,rows){
            if(err) {
                res.json({"Error" : true, "Message" : "Error executing MySQL query"});
            } else {
                res.json({"Error" : false, "Message" : "Success", "Users" : rows});
            }
        });
    });

    router.get("/lecture/datefrom/:Datefrom",function(req,res){
        var query = "SELECT * FROM ?? WHERE ??>?";
        var table = ["lecture","Date",req.params.Datefrom];
        query = mysql.format(query,table);
        connection.query(query,function(err,rows){
            if(err) {
                res.json({"Error" : true, "Message" : "Error executing MySQL query"});
            } else {
                res.json({"Error" : false, "Message" : "Success", "Users" : rows});
            }
        });
    });

    router.get("/lecture/dateto/:Dateto",function(req,res){
        var query = "SELECT * FROM ?? WHERE ??<?";
        var table = ["lecture","Date",req.params.Dateto];
        query = mysql.format(query,table);
        connection.query(query,function(err,rows){
            if(err) {
                res.json({"Error" : true, "Message" : "Error executing MySQL query"});
            } else {
                res.json({"Error" : false, "Message" : "Success", "Users" : rows});
            }
        });
    });

    


}

module.exports = REST_ROUTER;